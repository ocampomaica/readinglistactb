// Activity:

/*
	1. Create a student grading system using an arrow function. The grade categories are as follows: Failed(74 and below), Beginner (75-80), Developing (81-85), Above Average (86-90), Advanced(91-100) 

	Sample output in the console: Congratulations! Your quarterly average is 85. You have received a Developing" mark.
*/

// Code here:

const studentGrade = (average) => {
  if (average <= 74) {
    console.log(
      `Your quarterly average is ${average}. You're failed.`
    );
  } else if (average >= 75 && average <= 80) {
    console.log(
      `Congratulations! Your quarterly average is ${average}. You have received a "Beginner" mark.`
    );
  } else if (average >= 81 && average <= 85) {
    console.log(
      `Congratulations! Your quarterly average is ${average}. You have received a "Developing" mark.`
    );
  } else if (average >= 86 && average <= 90) {
    console.log(
      `Congratulations! Your quarterly average is ${average}. You have received  "Above Average" mark.`
    );
  } else {
    console.log(
      `Congratulations! Your quarterly average is ${average}. You have received  "Advanced" mark.`
    );
  }
};
studentGrade(85);
/*
	2. Create an odd-even checker that will check which numbers from 1-300 are odd and which are even,

	Sample output in the console: 
		1 - odd
		2 - even
		3 - odd
		4 - even
		5 - odd
		etc.
*/

// Code here:
console.log("");
function numCheck(num) {
  if (num > 300) {
    return "Invalid Input";
  }
  for (x = 1; x <= num; x++) {
    if (x % 2 === 0) {
      console.log(x + " - even");
    } else {
      console.log(x + " - odd");
    }
  }
}
numCheck(10);
/*
	3. Create a an object named ""hero"" and input the details using promp(). Here are the details needed: heroName, origin, description, skills(object which will contain 3 uinique skills). Convert hero JS object to JSON data format and log the output in the console.

	Sample output in the console:
		{
		        "heroName": "Aldous",
		        "origin: "Minoan Empire,
		        "description: "A guard of the Minos Labyrinth who kept his pledge even after the kingdom's fall.,
		        "skills": {
		                "skill1": "Soul Steal",
		                |"Skill2": "Explosion",
		                "Skill3": "Chase Fate"
		        }
		}
*/
//code here
console.log("")
let hero = {
    skills:{}
}

hero.heroName = prompt('Hero name:');
hero.origin = prompt('Hero origin:');
hero.description = prompt('Hero description:');
hero.skills.skill1 = prompt('Hero skill 1:');
hero.skills.skill2 = prompt('Hero skill 2:');
hero.skills.skill3 = prompt('Hero skill 3');
console.log(JSON.stringify(hero));